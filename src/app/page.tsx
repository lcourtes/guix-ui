"use client";

import {
  BuildSystem, ChangeEvent, License, PackageAttributes, PackageDescription, PackageSource, PackagesDb, PackagesList
} from '@/lib/types';
import { DefaultCMakePackage, DefaultEmptyPackage, DefaultHaskellPackage, DefaultHaskellPackageInputs, DefaultPackage, DefaultPackageInputs, DefaultPyprojectPackage, PACKAGES_URL, packageToString } from '@/lib/packages';
import { useCallback, useEffect, useMemo, useState } from 'react';

import Alert from '@mui/material/Alert';
import AppBar from '@mui/material/AppBar';
import BuildSystemInput from '@/components/inputs/BuildSystemInput';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import CssBaseline from '@mui/material/CssBaseline';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Unstable_Grid2';
import Help from '@/components/Help';
import IconButton from '@mui/material/IconButton';
import LicenseInput from '@/components/inputs/LicenseInput';
import LightModeIcon from '@mui/icons-material/LightMode';
import PackagesInputs from '@/components/inputs/PackagesInputs';
import Render from '@/components/Render';
import SourceInput from '@/components/inputs/SourceInput';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import ThemeProvider from '@mui/material/styles/ThemeProvider';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import createTheme from '@mui/material/styles/createTheme';
import styled from '@mui/material/styles/styled';
import { useCachedData } from '@/lib/hooks/useRemotePackages';
import useMediaQuery from '@mui/material/useMediaQuery';

function findPackage(db: PackagesDb, variable: string): [PackageAttributes, number] {
  const index = db.findIndex(({ variableName }) => variableName === variable);
  return [db[index], index];
}

const RedTypography = styled(Typography)(
  ({ theme }) => `
  color: ${theme.palette.error.main};`,
);

function ludoSaidItsCracraButRequired(db: PackagesDb): PackagesDb {
  // A better name for this function could be "pkgConfigHack", but let's face it,
  // it would be less funny.
  // Its purpose is to remove the '%pkg-config' variable to replace it by the
  // 'pkg-config', which is unfortunately widely used by packages but not
  // exposed through packages.json.
  const percentIndex = db.findIndex(({ variableName }) => variableName === '%pkg-config');
  const pkgConfig = {
    ...db[percentIndex],
    variableName: 'pkg-config',
  };
  db.splice(percentIndex, 1, pkgConfig);
  return db;
}

export default function Home() {
  const [name, setName] = useState<string>(DefaultPackage.name);
  const [version, setVersion] = useState<string>(DefaultPackage.version);
  const [buildSystem, setBuildSystem] = useState<BuildSystem>(DefaultPackage.buildSystem);
  const [source, setSource] = useState<PackageSource>(DefaultPackage.source);
  const [inputs, setInputs] = useState<PackagesList>(DefaultPackage.inputs);
  const [nativeInputs, setNativeInputs] = useState<PackagesList>(DefaultPackage.nativeInputs);
  const [propagatedInputs, setPropagatedInputs] = useState<PackagesList>(DefaultPackage.propagatedInputs);
  const [synopsis, setSynopsis] = useState<string>(DefaultPackage.synopsis);
  const [description, setDescription] = useState<string>(DefaultPackage.description);
  const [homepage, setHomepage] = useState<string>(DefaultPackage.homepage);
  const [license, setLicense] = useState<License>(DefaultPackage.license);

  const processData = (data: PackagesDb): PackagesDb =>
    ludoSaidItsCracraButRequired(data.map(({ name, variable_name, location, version }) => ({
      name,
      variableName: variable_name,
      location,
      version
    })).filter(({ variableName }) => variableName) as PackagesDb);

  const { data, resetAndFetchData, loading, error } =
    useCachedData<PackagesDb>("packages", PACKAGES_URL, [], processData);

  const packages = useMemo(() => data.map(packageToString).sort(),
    [data]);

  const packagesReady = !loading && !error && data.length > 0;

  const setPackage = useCallback((desc: PackageDescription) => {
    setName(desc.name);
    setVersion(desc.version);
    setBuildSystem(desc.buildSystem);
    setSource(desc.source);
    if (desc.name === 'hello-ui') {
      setInputs(DefaultPackageInputs.map((variable: string) => packageToString(...findPackage(data, variable))));
    } else if (desc.name === 'my-ghc-hs-conllu') {
      setInputs(DefaultHaskellPackageInputs.map((variable: string) => packageToString(...findPackage(data, variable))));
    } else {
      setInputs(desc.inputs);
    }
    setNativeInputs(desc.nativeInputs);
    setPropagatedInputs(desc.propagatedInputs);
    setSynopsis(desc.synopsis);
    setDescription(desc.description);
    setHomepage(desc.homepage);
    setLicense(desc.license);
  }, [
    setName, setVersion, setBuildSystem, setSource, setInputs,
    setNativeInputs, setPropagatedInputs, setSynopsis, setDescription,
    setHomepage, setLicense, data,
  ]);

  useEffect(() => {
    if (packagesReady) {
      setInputs(DefaultPackageInputs.map((variable: string) => packageToString(...findPackage(data, variable))));
    }
  }, [packagesReady, data]);

  const desc: PackageDescription = {
    name,
    version,
    source,
    buildSystem,
    inputs,
    nativeInputs,
    propagatedInputs,
    synopsis,
    description,
    homepage,
    license,
  };

  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  const [mode, setMode] = useState<'dark'|'light'>(prefersDarkMode ? 'dark' : 'light');
  const toggleMode = () => setMode((prev) => prev === 'dark' ? 'light' : 'dark');

  const theme = useMemo(() => createTheme({ palette: { mode } }), [mode]);
  const onMd = useMediaQuery(theme.breakpoints.up('md'));

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar variant="dense">
          <Typography variant="h6" component="div">
            Guix Packager — Write a package definition in a breeze
          </Typography>
          <Help />
          <IconButton color="inherit" onClick={toggleMode}>
            {mode === 'dark' ? (
              <LightModeIcon />
            ) : (
              <DarkModeIcon />
            )}
          </IconButton>
        </Toolbar>
      </AppBar>
      <Grid container spacing={2} sx={{m: 1}}>
        <Grid xs={12} md={6} container direction="column">
          <Stack direction={onMd ? 'row' : 'column'}>
            <Grid xs={12}>
              <TextField
                fullWidth
                label="Name"
                value={name}
                onChange={(event: ChangeEvent) => {
                  setName(event.target.value);
                }}
              />
            </Grid>
            <Grid xs={12}>
              <TextField
                fullWidth
                label="Version"
                value={version}
                onChange={(event: ChangeEvent) => {
                  setVersion(event.target.value);
                }}
              />
            </Grid>
            <Grid xs={12}>
              <LicenseInput license={license} setLicense={setLicense} />
            </Grid>
          </Stack>
          <Grid xs={12}>
            <TextField
              label="Synopsis"
              fullWidth
              value={synopsis}
              onChange={(event: ChangeEvent) => {
                setSynopsis(event.target.value);
              }}
            />
          </Grid>
          <Grid xs={12}>
            <TextField
              label="Description"
              value={description}
              fullWidth
              multiline
              onChange={(event: ChangeEvent) => {
                setDescription(event.target.value);
              }}
            />
          </Grid>
          <Grid xs={12}>
            <TextField
              label="Home page"
              value={homepage}
              fullWidth
              onChange={(event: ChangeEvent) => {
                setHomepage(event.target.value);
              }}
            />
          </Grid>
          <Grid xs={12}>
            <Stack direction="row" spacing={2}>
              <BuildSystemInput buildSystem={buildSystem} setBuildSystem={setBuildSystem} />
              <SourceInput
                name={name}
                source={source}
                setSource={setSource}
                version={version}
              />
            </Stack>
          </Grid>
          {(loading) && (
            <Grid xs={12} sx={{ display: 'flex', justifyContent: 'center' }}>
              <CircularProgress />
            </Grid>
          )}
          {error && (
            <Grid xs={12}>
              <Alert severity="error">{error}</Alert>
            </Grid>
          )}
          {packagesReady && (
            <PackagesInputs
              packages={packages}
              inputs={inputs}
              nativeInputs={nativeInputs}
              propagatedInputs={propagatedInputs}
              setInputs={setInputs}
              setNativeInputs={setNativeInputs}
              setPropagatedInputs={setPropagatedInputs}
            />
          )}
          {!loading && data.length === 0 && (
            <Grid xs={12}>
              <Button
                onClick={resetAndFetchData}
                variant="contained"
                fullWidth
                color="warning"
              >
              Add dependencies
              </Button>
            </Grid>
          )}
          <Grid xs={12} spacing={2}>
            <Divider variant="fullWidth" >
              <RedTypography variant="button">
                Danger zone
              </RedTypography>
            </Divider>
            <Button onClick={resetAndFetchData} sx={{mr: 2}} color="error">
              Reset package database
            </Button>
            <Button
              color="error"
              sx={{mr: 2}}
              onClick={() => setPackage(DefaultEmptyPackage)}
            >
              Reset to empty example
            </Button>
            <Button
              color="error"
              sx={{mr: 2}}
              onClick={() => setPackage(DefaultPackage)}
            >
              Reset to GNU+URL example
            </Button>
            <Button
              color="error"
              onClick={() => setPackage(DefaultCMakePackage)}
            >
              Reset to CMake+Git example
            </Button>
            <Button
              color="error"
              onClick={() => setPackage(DefaultHaskellPackage)}
            >
              Reset to Haskell example
            </Button>
            <Button
              color="error"
              onClick={() => setPackage(DefaultPyprojectPackage)}
            >
              Reset to Pyproject example
            </Button>
          </Grid>
        </Grid>
        <Grid xs={12} md={6}>
          <Render desc={desc} db={data} />
        </Grid>
      </Grid>
    </ThemeProvider>
  )
}
