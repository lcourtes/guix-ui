export interface PackageAttributes {
  name: string,
  version: string,
  location: string,
  variableName: string,
  variable_name?: string,
}

export type PackagesDb = Array<PackageAttributes>;

export type Method = 'git-fetch' | 'url-fetch' | 'hackage-fetch' | 'pypi-fetch'

// FIXME: find out something to interpolate stuff like 'version'.
// Maybe <<version>>.
export type UrlFetch = string;

export type GitFetch = {
  url: string,
  // tag or hash
  commit: string,
  recursive: boolean,
}

export type HackageFetch = {
  name: string,
}

export type PypiFetch = {
  name: string,
}

export type UrlType = UrlFetch | GitFetch | HackageFetch | PypiFetch;

export type PackageSource = {
  method: Method,
  url: UrlType,
  sha256: string,
}

export type License = {
  variableName: string,
  name: string,
  uri: string
}

export type SystemKind = 'gnu' | 'cmake' | 'pyproject' | 'haskell'

export interface BaseBuildSystem {
  kind: SystemKind,
  tests: boolean,
  testTarget: string,
  // FIXME: this is only for gnu/cmake/...
  variables: Array<string>,
}

export interface GnuBuildSystem extends BaseBuildSystem {
  kind: 'gnu',
}

export interface CMakeBuildSystem extends BaseBuildSystem {
  kind: 'cmake',
  buildType: string,
}

export interface PyprojectBuildSystem extends BaseBuildSystem {
  kind: 'pyproject',
}

export interface HaskellBuildSystem extends BaseBuildSystem {
  kind: 'haskell',
  // FIXME: UI for this
  haddock: boolean,
}

export type BuildSystem = GnuBuildSystem | CMakeBuildSystem | PyprojectBuildSystem | HaskellBuildSystem;

export type PackagesList = Array<string>;

export type PackageDescription = {
  name: string,
  version: string,
  source: PackageSource,
  buildSystem: BuildSystem,
  inputs: PackagesList,
  nativeInputs: PackagesList,
  propagatedInputs: PackagesList,
  synopsis: string,
  description: string,
  homepage: string,
  license: License,
}

export type ChangeEvent = React.ChangeEvent<HTMLInputElement>;
export type SetState<T> = React.Dispatch<React.SetStateAction<T>>;

export type StrToPkgTy = (str: string) => PackageAttributes;
