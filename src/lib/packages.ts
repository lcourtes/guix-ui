import { DefaultLicense, ExpatLicense } from '@/lib/licenses';
import { HackageFetch, PackageAttributes, PackageDescription, PypiFetch } from "@/lib/types";

export const PACKAGES_URL = 'https://guix.gnu.org/packages.json';

export function packageToString(pkg: PackageAttributes, index: number): string {
  const { name, version, variableName } = pkg;
  return `${name}|${version}|${variableName}|${index}`;
}

export function stringToAttributes(str: string): {
  name: string,
  version: string,
  variableName: string,
  index: number,
} {
  const parts = str.split('|');
  const [name, version, variableName, index] = parts;
  return {
    name,
    version,
    variableName,
    index: +index,
  };
}

export function hackageUri(hackage: HackageFetch, version: string) {
  const { name } = hackage;
  return `https://hackage.haskell.org/package/${name}/${name}-${version}.tar.gz`;
}

export function pypiUri(pypi: PypiFetch, version: string) {
  const { name } = pypi;
  return `https://files.pythonhosted.org/packages/source/${name[0]}/${name}/${name}-${version}.tar.gz`;
}

// This is not included in the DefaultPackage because we *dynamically* load
// the packages database, and therefore we cannot build the correct package
// object statically.
export const DefaultPackageInputs: Array<string> = ['gawk'];
export const DefaultHaskellPackageInputs: Array<string> = ['ghc-megaparsec', 'ghc-void'];

export const DefaultEmptyPackage: PackageDescription = {
  name: '',
  version: '',
  buildSystem: {
    kind: 'gnu',
    testTarget: 'check',
    tests: true,
    variables: [],
  },
  homepage: '',
  source: {
    method: 'url-fetch',
    url: '',
    sha256: '',
  },
  inputs: [],
  nativeInputs: [],
  propagatedInputs: [],
  synopsis: '',
  description: '',
  license: DefaultLicense,
};

export const DefaultPackage: PackageDescription = {
  name: "hello-ui",
  version: "2.10",
  buildSystem: {
    kind: "gnu",
    testTarget: "check",
    tests: true,
    variables: ["--enable-silent-rules"],
  },
  homepage: "https://www.gnu.org/software/hello/",
  source: {
    method: 'url-fetch',
    url: "mirror://gnu/hello/hello-2.10.tar.gz",
    sha256: '0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i',
  },
  inputs: [],
  nativeInputs: [],
  propagatedInputs: [],
  synopsis: "Hello, GNU world: An example GNU package",
  description: "Guess what GNU Hello prints!",
  license: DefaultLicense,
};

export const DefaultCMakePackage: PackageDescription = {
  name: "hello-ui-cmake",
  version: "1.0",
  buildSystem: {
    kind: "cmake",
    testTarget: "check",
    tests: true,
    buildType: "Release",
    variables: ["-DHELLO_WHO=Guix"],
  },
  homepage: "https://gitlab.inria.fr/viroulea/hello-cmake",
  source: {
    method: "git-fetch",
    url: {
      url: "https://gitlab.inria.fr/viroulea/hello-cmake",
      commit: "v1.0",
      recursive: false,
    },
    sha256: "1519qbj2063b4389ishxnnznk8f4qx4zcfg7ssskk7bp2w5jlccz",
  },
  inputs: [],
  nativeInputs: [],
  propagatedInputs: [],
  synopsis: "Hello world using CMake",
  description: "This is a simple configurable hello program.",
  license: DefaultLicense,
};

export const DefaultHaskellPackage: PackageDescription = {
  name: "my-ghc-hs-conllu",
  version: "0.1.5",
  buildSystem: {
    kind: "haskell",
    testTarget: "check",
    // FIXME: maybe tests? doesn't exist for haskell
    tests: false,
    haddock: true,
    variables: [],
  },
  homepage: "https://github.com/arademaker/hs-conllu",
  source: {
    method: "hackage-fetch",
    url: {
      name: 'hs-conllu',
    },
    sha256: "1azh4g5kdng8v729ldgblkmrdqrc501rgm9wwqx6gkqwwzn8w3r4",
  },
  inputs: [],
  nativeInputs: [],
  propagatedInputs: [],
  synopsis: "CoNLL-U validating parser and utils",
  description: "Utilities to parse, print, diff, and analyse data in CoNLL-U, a format used in linguistics to represent the syntactic annotation of sentences. See @url{https://universaldependencies.org/format.html}",
  license: DefaultLicense,
};

export const DefaultPyprojectPackage: PackageDescription = {
  name: "my-python-plotext",
  version: "5.2.8",
  buildSystem: {
    kind: "pyproject",
    // FIXME: target doesn't exist for pyproject
    testTarget: "check",
    // there are no tests
    tests: false,
    variables: [],
  },
  homepage: "https://github.com/piccolomo/plotext",
  source: {
    method: "pypi-fetch",
    url: {
      name: 'plotext',
    },
    sha256: "1gpy1z2i4vq1mcrhysxahz4339pbl9rzk58rf5m5gf5ym9xji6ii",
  },
  inputs: [],
  nativeInputs: [],
  propagatedInputs: [],
  synopsis: "Plots in the terminal",
  description: "Plotext lets you plot directly to the terminal.",
  license: ExpatLicense,
};
