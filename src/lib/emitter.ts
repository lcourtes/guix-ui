import {
  BuildSystem, CMakeBuildSystem, GitFetch, HackageFetch, Method, PackageAttributes, PackageDescription, PackageSource, PackagesDb, PypiFetch, UrlFetch
} from '@/lib/types';
import { stringToAttributes } from './packages';

const DEFAULT_MODULES = [
  '  #:use-module (guix)',
  '  #:use-module ((guix licenses) #:prefix license:)',
  '  #:use-module (gnu packages)',
];

function locationToUseModule(name: string): string {
  const srcIndex = name.indexOf('.scm');
  const modulePath = name.slice(0, srcIndex);
  return `  #:use-module (${modulePath.replaceAll('/', ' ')})`;
}

function isFirst<T>(value: T, index: number, array: Array<T>) {
  return array.indexOf(value) === index;
}

function EmitOpenMPISetupIfNeeded(packages: Array<PackageAttributes>): string {
  if (packages.filter(({ name }) => name === "openmpi").length === 0) {
    return '';
  } else {
    return "\n                     #:phases #~(modify-phases %standard-phases (add-before 'check 'mpi-setup #$%openmpi-setup))";
  }
}

function EmitDefaultUseModule(desc: PackageDescription) {
  const extraModules = [];
  let includeHaskell = false;
  switch (desc.buildSystem.kind) {
  case 'gnu':
    extraModules.push('  #:use-module (guix build-system gnu)');
    break;
  case 'cmake':
    extraModules.push('  #:use-module (guix build-system cmake)');
    break;
  case 'haskell':
    includeHaskell = true;
    break;
  case 'pyproject':
    extraModules.push('  #:use-module (guix build-system pyproject)');
    break;
  default:
    break;
  }
  switch (desc.source.method) {
  case 'git-fetch':
    extraModules.push('  #:use-module (guix git-download)');
    break;
  case 'pypi-fetch':
    extraModules.push('  #:use-module (guix build-system python)');
    break;
  case 'hackage-fetch':
    includeHaskell = true;
    break;
  }
  if (includeHaskell) {
    extraModules.push('  #:use-module (guix build-system haskell)');
  }
  return DEFAULT_MODULES.concat(extraModules);
}

function EmitUseModuleForPackages(packages: Array<PackageAttributes>): Array<string> {
  const modules = packages.map(({ location }) => location).map(locationToUseModule);
  return modules.filter(isFirst)
}

function EmitUseModule(desc: PackageDescription, packages: Array<PackageAttributes>): string {
  return [
    ...EmitDefaultUseModule(desc),
    ...EmitUseModuleForPackages(packages)
  ].join('\n');
}

function EmitInputs(name: string, inputs: Array<PackageAttributes>) {
  if (inputs.length === 0) {
    return '';
  }
  const names = inputs.map(({ variableName }) => variableName);
  return `
    (${name}
     (list ${names.join(' ')}))`
}

//type OptionEmitterTy = (val: any): string;

type OptionEmitter<T> = (value: T) => string;

function EmitOptionalValue<T>(val: T, defaultValue: T, emitDefault: boolean, emitter: OptionEmitter<T>) {
  if (emitDefault || val !== defaultValue) {
    return emitter(val);
  } else {
    return '';
  }
}

function EmitGitRecursive(recursive: boolean): string {
  return `\n              (recursive? ${recursive ? '#t' : '#f'})`;
}

function EmitGitUrl(url: GitFetch, emitDefault: boolean): string {
  return `(uri (git-reference
              (url "${url.url}")${EmitOptionalValue(url.recursive, false, emitDefault, EmitGitRecursive)}
              (commit "${url.commit}")))
        (file-name (git-file-name name version))`;
}

function EmitUrl(url: UrlFetch): string {
  return `(uri "${url}")`;
}

function EmitHackageUrl(url: HackageFetch): string {
  return `(uri (hackage-uri "${url.name}" version))`;
}

function EmitPypiUrl(url: PypiFetch): string {
  return `(uri (pypi-uri "${url.name}" version))`;
}

function UrlToString(source: PackageSource, emitDefault: boolean): string {
  switch (source.method) {
  case 'url-fetch':
    return EmitUrl(source.url as UrlFetch);
  case 'hackage-fetch':
    return EmitHackageUrl(source.url as HackageFetch);
  case 'pypi-fetch':
    return EmitPypiUrl(source.url as PypiFetch);
  case 'git-fetch':
    return EmitGitUrl(source.url as GitFetch, emitDefault);
  default:
    return '';
  }
}

function MethodToString(method: Method) {
  switch(method) {
  case 'url-fetch':
  case 'hackage-fetch':
  case 'pypi-fetch':
    return 'url-fetch';
  case 'git-fetch':
    return 'git-fetch';
  default:
    return 'unknown';
  }
}

function EmitSource(source: PackageSource, emitDefault: boolean): string {
  return `(source
      (origin
        (method ${MethodToString(source.method)})
        ${UrlToString(source, emitDefault)}
        (sha256 (base32 "${source.sha256}"))))`;
}

function EmitArgumentsForCMake(buildSystem: CMakeBuildSystem): string {
  return `\n                     #:build-type "${buildSystem.buildType}"`;
}

function EmitSetuptools(setuptools: boolean): string {
  return `\n                     #:use-setuptools? ${setuptools ? '#t' : '#f'}`;
}

// FIXME: use this when implementing PythonBuildSystem
//function EmitArgumentsForPython(buildSystem: PythonBuildSystem, emitDefault: boolean): string {
//return EmitOptionalValue(buildSystem.setuptools, true, emitDefault, EmitSetuptools);
//}

function EmitConfigureFlags(buildSystem: BuildSystem): string {
  const variables = buildSystem.variables.filter(v => v.length >= 0).map(v => `"${v}"`);
  if (buildSystem.variables.length === 0) {
    return '';
  };
  return `#:configure-flags #~(list ${variables.join("\n")})`;
}

function EmitTestTarget(target: string): string {
  return `\n                     #:test-target "${target}"`;
}

function EmitEnableTests(tests: boolean): string {
  return `\n                     #:tests? ${tests ? '#t' : '#f'}`;
}

function ArgsToString(buildSystem: BuildSystem, emitDefault: boolean): string {
  switch (buildSystem.kind) {
  case 'cmake':
    return EmitArgumentsForCMake(buildSystem as CMakeBuildSystem);
  default:
    return '';
  }
}

function EmitArgumentsForBuildSystem(buildSystem: BuildSystem, emitDefault: boolean): string {
  const argsString = ArgsToString(buildSystem, emitDefault);
  const defaultTestValue = buildSystem.kind === 'haskell' ? false : true;
  // FIXME: quick and dirty, ideally this should be reflected in the type!
  const testTarget = buildSystem.kind === 'pyproject' ? '' : EmitOptionalValue(buildSystem.testTarget, 'check', emitDefault, EmitTestTarget);
  return `${EmitConfigureFlags(buildSystem)}${testTarget}${EmitOptionalValue(buildSystem.tests, defaultTestValue, emitDefault, EmitEnableTests)}${argsString}`;
}

function EmitArguments(desc: PackageDescription, packages: Array<PackageAttributes>, emitDefault: boolean): string {
  return `(arguments (list ${EmitArgumentsForBuildSystem(desc.buildSystem, emitDefault)}${EmitOpenMPISetupIfNeeded(packages)}))`;
}

function BuildSystemToString(buildSystem: BuildSystem): string {
  switch(buildSystem.kind) {
  case 'cmake':
    return 'cmake-build-system';
  case 'gnu':
    return 'gnu-build-system';
  case 'pyproject':
    return 'pyproject-build-system';
  case 'haskell':
    return 'haskell-build-system';
  default:
    return 'unknown';
  }
}

export function EmitPackage(desc: PackageDescription, db: PackagesDb, emitDefault: boolean): string {
  const stringToPackage = (str: string): PackageAttributes => {
    const { index } = stringToAttributes(str);
    return db[index];
  };
  const inputs = [...desc.inputs].map(stringToPackage);
  const nativeInputs = [...desc.nativeInputs].map(stringToPackage);
  const propagatedInputs = [...desc.propagatedInputs].map(stringToPackage);
  const packages = [...inputs, ...nativeInputs];

  return `(define-module (guix-packager)
${EmitUseModule(desc, packages)})

(define-public ${desc.name}
  (package
    (name "${desc.name}")
    (version "${desc.version}")
    ${EmitSource(desc.source, emitDefault)}${EmitInputs('native-inputs', nativeInputs)}${EmitInputs('inputs', inputs)}${EmitInputs('propagated-inputs', propagatedInputs)}
    (build-system ${BuildSystemToString(desc.buildSystem)})
    ${EmitArguments(desc, packages, emitDefault)}
    (home-page "${desc.homepage}")
    (synopsis "${desc.synopsis}")
    (description "${desc.description}")
    (license license:${desc.license.variableName})))

;; This allows you to run guix shell -f guix-packager.scm.
;; Remove this line if you just want to define a package.
${desc.name}`;
}
