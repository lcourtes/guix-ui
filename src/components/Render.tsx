import { ChangeEvent, PackageDescription, PackagesDb } from '@/lib/types';
import { useCallback, useState } from 'react';

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { EmitPackage } from '@/lib/emitter';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import SyntaxHighlighter from '@/components/SyntaxHighlighter';

export default function Render({ desc, db }: {
  desc: PackageDescription,
  db: PackagesDb,
}) {
  const [emitDefault, setEmitDefault] = useState(false);
  const onChangeEmitDefault = useCallback((event: ChangeEvent) =>
    setEmitDefault(event.target.checked), [setEmitDefault]);

  const packageAsString = EmitPackage(desc, db, emitDefault);
  const onClick = () => {
    const blob = new Blob([packageAsString], { type: "text/plain" });
    const link = document.createElement("a");
    link.download = "guix-packager.scm";
    link.href = URL.createObjectURL(blob);
    link.click();
  }

  return (
    <>
      <FormControlLabel
        control={
          <Switch
            checked={emitDefault}
            onChange={onChangeEmitDefault}
            name="default values" />
        }
        label="Emit default values"
      />
      <Button variant="contained" onClick={onClick}>
        Download scm
      </Button>
      <Box
        sx={{
          fontSize: "85%",
        }}
      >
        <SyntaxHighlighter
          text={packageAsString}
          plugins={['line-numbers', 'match-braces', 'rainbow-braces']}
          language="lisp"
        />
      </Box>
    </>
  );
}
