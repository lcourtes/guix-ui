import {
  ChangeEvent, GitFetch, HackageFetch, Method, PackageSource, PypiFetch, SetState, UrlFetch, UrlType
} from '@/lib/types';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { hackageUri, pypiUri } from '@/lib/packages';
import { useCallback, useEffect, useMemo, useState } from 'react';

import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import CloseIcon from '@mui/icons-material/Close';
import Dialog from '@mui/material/Dialog';
import EditIcon from '@mui/icons-material/Edit';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Unstable_Grid2';
import IconButton from '@mui/material/IconButton';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Stack from '@mui/material/Stack';
import SyntaxHighlighter from '@/components/SyntaxHighlighter';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

type InputProps = {
  name: string,
  source: PackageSource,
  setSource: SetState<PackageSource>,
  version: string,
}

type FormProps = InputProps & {
  open: boolean,
  setOpen: SetState<boolean>,
}

const methods = ["url-fetch", "git-fetch", "hackage-fetch", "pypi-fetch"];

function UrlFetchForm({ url, setUrl }: {
  url: UrlFetch,
  setUrl: SetState<UrlType>,
}) {
  return (
    <FormControl fullWidth variant="standard" sx={{mb: 2}}>
      <TextField
        label="Url"
        value={url}
        onChange={(event: ChangeEvent) => {
          setUrl(event.target.value);
        }}
      />
    </FormControl>
  );
}

function PackageFetchForm({ pkg, setUrl }: {
  pkg: HackageFetch | PypiFetch,
  setUrl: SetState<UrlType>,
}) {
  const { name } = pkg;
  const onChangeName = useCallback((event: ChangeEvent) =>
    setUrl({ name: event.target.value }), [setUrl]);
  return (
    <FormControl fullWidth variant="standard" sx={{mb: 2}}>
      <TextField
        label="Package name"
        value={name}
        onChange={onChangeName}
      />
    </FormControl>
  );
}

function GitFetchForm({ gitUrl, setUrl }: {
  gitUrl: GitFetch,
  setUrl: SetState<UrlType>,
}) {
  const { url, commit, recursive } = gitUrl;
  const onChangeUrl = useCallback((event: ChangeEvent) =>
    setUrl({ commit, url: event.target.value, recursive }),
  [commit, recursive, setUrl]);
  const onChangeCommit = useCallback((event: ChangeEvent) =>
    setUrl({ commit: event.target.value, url, recursive }),
  [url, recursive, setUrl]);
  const onChangeRecursive = useCallback((event: ChangeEvent) =>
    setUrl({ recursive: event.target.checked, url, commit }),
  [commit, url, setUrl]);
  return (
    <>
      <FormControl fullWidth>
        <TextField
          label="Url"
          value={url}
          onChange={onChangeUrl}
        />
      </FormControl>
      <FormControl fullWidth>
        <TextField
          label="Tag or branch"
          value={commit}
          onChange={onChangeCommit}
        />
      </FormControl>
      <FormControlLabel control={<Checkbox
        checked={recursive}
        onChange={onChangeRecursive}
      />} label="Clone submodules" />
    </>
  );
}

function getUrlFetchHelp(url: UrlFetch) {
  return `guix download ${url}`
}

function getGitFetchHelp(url: GitFetch) {
  const recurse = url.recursive ? '-r ' : '';
  return `guix download ${recurse}--commit=${url.commit} ${url.url}`;
}

function getFetchHelp(method: Method, url: UrlType, version: string) {
  switch (method) {
  case 'url-fetch':
    return `guix download ${url as UrlFetch}`;
  case 'git-fetch':
    return getGitFetchHelp(url as GitFetch);
  case 'hackage-fetch':
    return `guix download ${hackageUri(url as HackageFetch, version)}`
  case 'pypi-fetch':
    return `guix download ${pypiUri(url as PypiFetch, version)}`
  default:
    return 'unsupported fetch method';
  }
}

function HashHelp({ method, url, version }: {
  method: Method, url: UrlType, version: string
}) {
  return (
    <div
      onClick={() => {
        navigator.clipboard.writeText(getFetchHelp(method, url, version))
      }}>
      <Typography variant="body2">
        You may compute the sha256 by running the following command(s) (click to copy):
      </Typography>
      <SyntaxHighlighter
        text={getFetchHelp(method, url, version)}
        plugins={[]}
        language="bash"
      />
    </div>
  );
}

function stripPrefix(name: string, type: 'pypi' | 'hackage') {
  switch (type) {
  case 'pypi':
    return name.replace(/^python-/, '');
  case 'hackage':
    return name.replace(/^ghc-/, '');
  default:
    return name;
  }
}

function Form({
  name, open, setOpen, source, setSource, version
}: FormProps) {
  const [method, setMethod] = useState<Method>(source.method);
  const [url, setUrl] = useState<UrlType>(source.url);
  const [sha256, setSha256] = useState<string>(source.sha256);

  const defaultUrl: { [key: string]: UrlType } = useMemo(() => {
    const val = { [source.method]: source.url };
    if (source.method === "url-fetch") {
      val["git-fetch"] = {
        url: source.url as string,
        commit: version,
        recursive: false,
      };
      val["hackage-fetch"] = { name: stripPrefix(name, 'hackage') };
      val["pypi-fetch"] = { name: stripPrefix(name, 'pypi') };
    } else if (source.method === "git-fetch") {
      val["url-fetch"] = (source.url as GitFetch).url;
      val["hackage-fetch"] = { name: stripPrefix(name, 'hackage') };
      val["pypi-fetch"] = { name: stripPrefix(name, 'pypi') };
    } else if (source.method === "hackage-fetch") {
      val["url-fetch"] = hackageUri(source.url as HackageFetch, version);
      val["git-fetch"] = {
        url: hackageUri(source.url as HackageFetch, version),
        commit: version,
        recursive: false,
      };
      val["pypi-fetch"] = { name: stripPrefix(name, 'pypi') };
    } else if (source.method === "pypi-fetch") {
      val["url-fetch"] = pypiUri(source.url as PypiFetch, version);
      val["git-fetch"] = {
        url: pypiUri(source.url as PypiFetch, version),
        commit: version,
        recursive: false,
      };
      val["hackage-fetch"] = { name: stripPrefix(name, 'hackage') };
    }
    return val;
  }, [source, name, version]);

  const setFromSource = useCallback(() => {
    setMethod(source.method);
    setUrl(source.url);
    setSha256(source.sha256);
  }, [source, setMethod, setUrl, setSha256]);

  const handleClose = useCallback(() => {
    setFromSource();
    setOpen(false);
  }, [setFromSource, setOpen]);

  // Clear the sha256 if the url has changed.
  // It makes it clear to the user that the sha is not precomputed (except in
  // the cases of the examples), so that they actually fill it in.
  useEffect(() => {
    if (url !== source.url) {
      setSha256('')
    }
  }, [setSha256, url, source.url]);

  useEffect(setFromSource, [setFromSource]);

  const onChange = useCallback((event: SelectChangeEvent) => {
    setMethod(event.target.value as Method);
    setUrl(defaultUrl[event.target.value as Method]);
  }, [setMethod, setUrl, defaultUrl]);

  const onChangeSha = useCallback((event: ChangeEvent) =>
    setSha256(event.target.value), [setSha256]);

  const save = () => {
    setSource({
      method,
      url,
      sha256,
    });
    setOpen(false);
  }

  return (
    <Dialog open={open} onClose={handleClose} fullScreen>
      <Grid container sx={{m:2}}>
        <Grid sm={12} sx={{mb:2}}>
          <Stack direction="row">
            <Typography sx={{ flex: 1 }} variant="h4" component="div">
            Edit source
            </Typography>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
          </Stack>
        </Grid>
        <Grid sm={12}>
          <FormControl sx={{mb: 2}}>
            <InputLabel>Method</InputLabel>
            <Select
              label="Method"
              value={method}
              onChange={onChange}
            >
              {methods.map(value => (
                <MenuItem value={value} key={value}>{value}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid sm={12}>
          <Stack direction="column" spacing={2}>
            {method === "url-fetch" && (
              <UrlFetchForm url={url as UrlFetch} setUrl={setUrl} />
            )}
            {method === "git-fetch" && (
              <GitFetchForm gitUrl={url as GitFetch} setUrl={setUrl} />
            )}
            {method === "hackage-fetch" && (
              <PackageFetchForm pkg={url as HackageFetch} setUrl={setUrl} />
            )}
            {method === "pypi-fetch" && (
              <PackageFetchForm pkg={url as PypiFetch} setUrl={setUrl} />
            )}
            <FormControl fullWidth variant="standard">
              <TextField
                label="sha256"
                value={sha256}
                onChange={onChangeSha}
              />
            </FormControl>
            <HashHelp method={method} url={url} version={version} />
          </Stack>
        </Grid>
        <Grid sm={12} sx={{mt: 2}}>
          <Button variant="contained" onClick={save}>Save</Button>
          <Button onClick={handleClose} sx={{ml: 5}}>Cancel</Button>
        </Grid>
      </Grid>
    </Dialog>
  );
}

export default function SourceInput({
  name,
  source,
  setSource,
  version
}: InputProps) {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Button
        fullWidth
        variant="contained"
        startIcon={<EditIcon />}
        onClick={() => setOpen(true)}
      >
        Customize source
      </Button>
      <Form
        name={name}
        open={open}
        setOpen={setOpen}
        source={source}
        setSource={setSource}
        version={version}
      />
    </>
  );
}
