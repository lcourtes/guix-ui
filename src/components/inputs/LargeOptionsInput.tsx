import * as React from 'react';

import Autocomplete, { autocompleteClasses } from '@mui/material/Autocomplete';
import { ListChildComponentProps, VariableSizeList } from 'react-window';
import Chip from '@mui/material/Chip';
import Popper from '@mui/material/Popper';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { matchSorter } from 'match-sorter';
import { stringToAttributes } from '@/lib/packages';
import { styled } from '@mui/material/styles';


const LISTBOX_PADDING = 8; // px

function packageStringToLabel(str: string): string {
  const { name, version, variableName } = stringToAttributes(str);
  return `${variableName} (${name}@${version})`;
}

function renderRow(props: ListChildComponentProps) {
  const { data, index, style } = props;
  const dataSet = data[index];
  const inlineStyle = {
    ...style,
    top: (style.top as number) + LISTBOX_PADDING,
  };
  return (
    <Typography component="li" {...dataSet[0]} key={dataSet[1]} noWrap style={inlineStyle}>
      {packageStringToLabel(dataSet[1])}
    </Typography>
  );
}

function useResetCache(data: any) {
  const ref = React.useRef<VariableSizeList>(null);
  React.useEffect(() => {
    if (ref.current != null) {
      ref.current.resetAfterIndex(0, true);
    }
  }, [data]);
  return ref;
}

const OuterElementContext = React.createContext({});

// eslint-disable-next-line react/display-name
const OuterElementType = React.forwardRef<HTMLDivElement>((props, ref) => {
  const outerProps = React.useContext(OuterElementContext);
  return <div ref={ref} {...props} {...outerProps} />;
});

const ListboxComponent = React.forwardRef<
  HTMLDivElement,
  React.HTMLAttributes<HTMLElement>
>(function ListboxComponent(props, ref) {
  const { children, ...other } = props;

  const itemCount = (children as React.ReactElement[]).length;

  const getChildSize = () => 48;

  const getHeight = () => Math.min(8, itemCount) * 48;

  const gridRef = useResetCache(itemCount);

  return (
    <div ref={ref}>
      <OuterElementContext.Provider value={other}>
        <VariableSizeList
          itemData={children}
          height={getHeight() + 2 * LISTBOX_PADDING}
          width="100%"
          ref={gridRef}
          outerElementType={OuterElementType}
          innerElementType="ul"
          itemSize={() => 48}
          overscanCount={5}
          itemCount={itemCount}
        >
          {renderRow}
        </VariableSizeList>
      </OuterElementContext.Provider>
    </div>
  );
});

const StyledPopper = styled(Popper)({
  [`& .${autocompleteClasses.listbox}`]: {
    boxSizing: 'border-box',
    '& ul': {
      padding: 0,
      margin: 0,
    },
  },
});

type Props = {
  options: Array<string>,
  value: Array<string>,
  label: string,
  placeholder: string,
  onChange: any,
  helperText?: string,
};

export default function LargeOptionsInput({
  options, value, onChange, label, placeholder, helperText
}: Props) {
  return (
    <Autocomplete
      multiple
      id="tags-outlined"
      options={options}
      value={value}
      onChange={onChange}
      filterSelectedOptions
      filterOptions={(options, { inputValue }) => matchSorter(options, inputValue)}
      limitTags={2}
      disableListWrap
      PopperComponent={StyledPopper}
      ListboxComponent={ListboxComponent}
      renderOption={(props, option, state) => [props, option] as React.ReactNode}
      renderTags={(tagValue, getTagProps) => (
        tagValue.map((option, index) => (
          <Chip {...getTagProps({ index })} key={index} label={packageStringToLabel(option)} />
        ))
      )}
      renderInput={(params) => {
        return (
          <TextField
            {...params}
            label={label}
            helperText={helperText}
            placeholder={placeholder}
          />
        )}}
    />
  )
}
